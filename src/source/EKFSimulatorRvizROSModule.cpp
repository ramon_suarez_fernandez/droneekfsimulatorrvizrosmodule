#include "EKFSimulatorRvizROSModule.h"

using namespace visualization_msgs;

droneMsgsROS::dronePose DroneInit;


Marker makeSphere(InteractiveMarker &msg )
{
  Marker marker;
  marker.type = Marker::SPHERE;
  marker.scale.x = msg.scale * 0.65;
  marker.scale.y = msg.scale * 0.65;
  marker.scale.z = msg.scale * 0.65;
  marker.color.r = 1.0;
  marker.color.g = 0.95686274509;
  marker.color.b = 0.30980392156;
  marker.color.a = 1.0;

  return marker;
}


void DroneRvizDisplay::frameCallback(const droneMsgsROS::dronePose &pose_euler)
{

  static tf::TransformBroadcaster br;
  tf::Transform t;
  //ROS_INFO("TF sent");
  ros::Time time = ros::Time::now();

  t.setOrigin(tf::Vector3(double(pose_euler.x),double(pose_euler.y),double(pose_euler.z)));
  t.setRotation(tf::createQuaternionFromRPY(pose_euler.roll, pose_euler.pitch, pose_euler.yaw));
  br.sendTransform(tf::StampedTransform(t, time, "base_link",std::string("moving_frame_EKF")));
  
  return;
}


void DroneRvizDisplay::makeMovingMarker( const tf::Vector3& position , std::string name, std::string frame_id_in)
{
  std::string frame_id = frame_id_in+"_EKF";
  InteractiveMarker int_marker;
  int_marker.header.frame_id = frame_id;
  tf::pointTFToMsg(position, int_marker.pose.position);
  int_marker.scale = 1;

  int_marker.name = name;
  int_marker.description = name;

  InteractiveMarkerControl control;

  control.orientation.w = 1;
  control.orientation.x = 1;
  control.orientation.y = 0;
  control.orientation.z = 0;
  control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(control);

  control.interaction_mode = InteractiveMarkerControl::MOVE_PLANE;
  control.always_visible = true;
  control.markers.push_back( makeSphere(int_marker) );
  int_marker.controls.push_back(control);

  server->insert(int_marker);
  ROS_INFO("Created Marker");

}
// %EndTag(Moving)%

void DroneRvizDisplay::open(ros::NodeHandle &nIn)
{

    n=nIn;
    ROS_INFO("Subscribed to EKF Drone Pose");
    sub = n.subscribe(DRONE_STATE_ESTIMATOR_INTERFACE_POSE_SUBSCRIPTION_GMR, 1, &DroneRvizDisplay::frameCallback, this);

}



void DroneRvizDisplay::ServerResetNew()
{
    ROS_INFO("reset new");
    server.reset( new interactive_markers::InteractiveMarkerServer("EKF_controls","",false) );
    return;

}

void DroneRvizDisplay::ServerReset()
{
    ROS_INFO("reset");
    server.reset();
    return;
}

void DroneRvizDisplay::ServerApplyChanges()
{
   ROS_INFO("changes");
   server->applyChanges();
   return;
}
